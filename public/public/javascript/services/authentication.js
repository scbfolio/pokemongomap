(function () {
    'use strict';

    angular
        .module('app')
        .factory('AuthenticationService', AuthenticationService);

    function AuthenticationService($rootScope, $firebaseAuth, $firebaseArray, $timeout, $state, $q, UsersService) {
        var scope = this;
        var service = {};

        var ref = SETTINGS.FIREBASE_REF;
        scope.auth = $firebaseAuth();

        service.GetFirebase     = GetFirebase;
        service.GetAuth         = GetAuth;
        service.GetCurrentState = GetCurrentState;

        service.SetUserOnline   = SetUserOnline;

        service.Register        = Register;
        service.Login           = Login;
        service.Logout          = Logout;
        service.ForgotPassword  = ForgotPassword;
        service.UpdatePassword  = UpdatePassword;
        service.LogWithGoogle   = LogWithGoogle;
        service.LogWithLinkedIn = LogWithLinkedIn;

        service.SearchUserID = SearchUserID;

        if (!$rootScope.globals) {
            $rootScope.globals = {};
        }

        scope.auth.$onAuthStateChanged(function(authData) {
            if (authData) {
                if (service.userSession) {service.userSession.$destroy();}
                $rootScope.globals.authLoaded = false;

                if (authData.password && authData.password.isTemporaryPassword) {
                    if ($rootScope.loginData) {
                        $rootScope.temporaryPassword = $rootScope.loginData.password;
                        delete $rootScope.loginData;
                    } else {
                        service.Logout();
                        return;
                    }
                }

                service.SearchUserID(authData).then(function(data) {
                    if (!data.canSigned) {
                        service.Logout();
                        $timeout(function() {
                            $rootScope.showAuthError('ALREADY_CREATED');
                        }, 100);
                        return;
                    }

                    service.userSession = $firebaseArray(new Firebase(SETTINGS.FIREBASE_URL + 'users-session/' + data.userID));
                    service.userSession.$loaded().then(function() {
                        service.userSession.$watch(function(e) {
                            if (e.event == "child_added") {
                                service.Logout();
                            }
                        });
                    });

                    $rootScope.globals.userId = data.userID;
                    $rootScope.globals.user = UsersService.GetByID(data.userID);
                    $rootScope.globals.user.$loaded().then(function () {
                        $rootScope.globals.auth = authData;
                        service.SetUserOnline($rootScope.globals.user.$id);
                        $rootScope.globals.authLoaded = true;

                        if ($state.includes('app.auth')) {
                            if ($rootScope.redirectState) {
                                $state.go($rootScope.redirectState.name, $rootScope.redirectState.params);
                                delete $rootScope.redirectState;
                            } else {
                                $state.go('app.accountCompletion.businessesList');
                            }
                        }
                    });
                });
            } else {
                delete $rootScope.temporaryPassword;
                delete $rootScope.loginData;
            }
        });

        //me.do for test purpose remove after dev
        window.Logout = Logout;

        return service;

        function GetFirebase() {
            return ref;
        }

        function GetAuth() {
            return scope.auth;
        }

        function GetCurrentState() {
            return ref.getAuth();
        }

        function SetUserOnline(uid) {
            scope.onlineRef = new Firebase(SETTINGS.FIREBASE_URL + 'online/' + uid);
            scope.onlineRef.set("online");
            scope.onlineRef.onDisconnect().set("offline");

            var ref = new Firebase(SETTINGS.FIREBASE_URL + 'users-session/' + uid);
            scope.sessionRef = ref.push();
            scope.sessionRef.child('begin').set(Firebase.ServerValue.TIMESTAMP);
            scope.sessionRef.child('leave').onDisconnect().set(Firebase.ServerValue.TIMESTAMP);

            /* me.do More status if we add idle.js */
            document.onIdle = function () {
                scope.onlineRef.set('idle');
            };

            document.onAway = function () {
                scope.onlineRef.set('away');
            };

            document.onBack = function (isIdle, isAway) {
                scope.onlineRef.set('online');
            };
        }

        function Register(data) {
            var deferred = $q.defer();

            UsersService.GetByEmail(data.email).once('value', function (snapshot) {
                if (snapshot.val() === null) {
                    scope.auth.$createUser(data)
                        .then(function () {
                            service.Login(data)
                                .then(function (auth) {
                                    deferred.resolve(auth);
                                })
                                .catch(function (error) {
                                    deferred.reject(error);
                                });
                        })
                        .catch(function (error) {
                            deferred.reject(error);
                        });
                } else {
                    deferred.reject("ALREADY_CREATED");
                }
            });

            return deferred.promise;
        }

        function Login(data) {
            var deferred = $q.defer();

            $rootScope.loginData = data;
            firebase.auth().createUserWithEmailAndPassword(data.email, data.password)
                .then(function(auth) {
                    deferred.resolve(auth);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function Logout() {
            if (service.userSession) {
                service.userSession.$destroy();
            }

            if (scope.onlineRef) {
                scope.onlineRef.set("offline");
                scope.sessionRef.child('leave').set(Firebase.ServerValue.TIMESTAMP);
            }

            scope.auth.$unauth();
        }

        function ForgotPassword(data) {
            var deferred = $q.defer();

            scope.auth.$resetPassword(data)
                .then(function () {
                    deferred.resolve();
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function UpdatePassword(data) {
            var deferred = $q.defer();

            scope.auth.$changePassword(data)
                .then(function (result) {
                    deferred.resolve(result);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function LogWithGoogle() {
            var deferred = $q.defer();

            scope.auth.$authWithOAuthPopup("google", {scope: "email"})
                .then(function (auth) {
                    deferred.resolve(auth);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function LogWithLinkedIn(token) {
            var deferred = $q.defer();

            scope.auth.$authWithCustomToken(token)
                .then(function(auth) {
                    deferred.resolve(auth);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function SearchUserID(authData) {
            var deferred = $q.defer();

            var id;
            var email;
            if (authData.auth.fb_id) {
                id    = authData.auth.fb_id;
                email = authData.auth.token.email;
            } else {
                id    = authData.auth.uid;
                email = authData[authData.provider].email;
            }

            ref.child('providers/' + id).once('value', function(snapshot) {
                if (snapshot.val()) {
                    deferred.resolve({userID: snapshot.val(), canSigned: true});
                    return;
                }

                UsersService.GetByEmail(email).once('value', function(snapshot) {
                    if (snapshot.val()) {
                        deferred.resolve({userID: Object.keys(snapshot.val())[0], canSigned: false});
                        return;
                    }

                    UsersService.Create(authData).then(function(userID) {
                        deferred.resolve({userID: userID, canSigned: true, created: true});
                    });
                });
            });

            return deferred.promise;
        }

        function linkAccountToAnotherMedia(userID, provider, uid) {
            var updates = {};
            updates["users/" + userID + "/providers/" + provider] = uid;
            updates['providers/' + uid] = userID;
            ref.update(updates).then(function () {
            });
        }
    }
})();