(function () {
    'use strict';

    angular
        .module('app')
        .factory('ProvidersService', ProvidersService);

    function ProvidersService($rootScope, $q) {
        var scope   = this;
        var service = {};

        var ref = SETTINGS.FIREBASE_REF;

        service.AddProvider = AddProvider;

        return service;

        function AddProvider(authID, uID) {
            var deferred = $q.defer();
            ref.child('providers/'+ authID).set(uID)
                .then(function(){
                    deferred.resolve();
                });
            return deferred.promise;
        }
    }
})();