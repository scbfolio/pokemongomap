(function () {
    'use strict';

    angular
        .module('app')
        .factory('UsersService', UsersService);

    function UsersService($rootScope, $firebaseArray, $q, UserFactory, ProvidersService) {
        var scope    = this;
        var service  = {};
        var ref      = SETTINGS.FIREBASE_REF;
        scope.data   = $firebaseArray(ref.child("users/"));

        service.Create     = Create;
        service.UpdateRole = UpdateRole;
        service.GetAll     = GetAll;
        service.GetByID    = GetByID;
        service.GetByEmail = GetByEmail;

        return service;

        function Create(authData) {
            var deferred = $q.defer();
            var newID = ref.child("users").push().key();
            var verificationID = ref.child("users-verification").push().key();

            var userData = {};
            userData.providers = {};

            var provider;
            if (authData.provider == "custom") {
                provider = authData.auth.fb_id.split('|')[0];
                userData.providers[provider] = authData.auth.fb_id;
                userData.email     =authData.auth.token.email;
            } else {
                provider = authData.provider;
                userData.providers[authData.provider] = authData.auth.uid;
                userData.email     = authData[authData.provider].email;
            }

            userData.status    = "active";
            userData.role      = "10";
            userData.verified  = true; // me.do if we go with verification email false
            userData.created   = Firebase.ServerValue.TIMESTAMP;
            userData.emails_preferences = {stand_up: true};

            if (provider == "linkedin") {
                var providerData    = $rootScope.stockedUserProfile;
                userData.firstname  = providerData.given_name;
                userData.lastname   = providerData.family_name;

                userData.language   = "en";
                userData.picture    = providerData.picture;

                console.log(providerData.positions);

                delete $rootScope.stockedUserProfile;
            } else if (provider == "google") {
                var providerData    = authData[authData.provider].cachedUserProfile;
                userData.firstname  = providerData.given_name;
                userData.lastname   = providerData.family_name;
                userData.gender     = providerData.gender;

                userData.language   = providerData.locale;
                userData.picture    = providerData.picture;
            } else if (provider == "password") {
                if ($rootScope.RegisterData) {
                    userData.firstname  = $rootScope.RegisterData.firstname;
                    userData.lastname   = $rootScope.RegisterData.lastname;
                }
                userData.language   = "en";
                userData.picture    = null;

                delete $rootScope.RegisterData;
            }


            if (userData.firstname && userData.lastname) {
                userData.initials = userData.firstname.charAt(0).toUpperCase() + userData.lastname.charAt(0).toUpperCase();
            }

            if (userData.picture && provider == "google") {
                convertImgToDataURLviaCanvas(userData.picture, function(base64Img){
                    var image = new Image();
                    image.src = base64Img;
                    var rgbImg = getAverageRGB(image);

                    // replace cause its base google image
                    if (rgbImg.r == 130 && rgbImg.g == 169 && rgbImg.b == 245) {
                        userData.picture = null;
                    }

                    endCreate(newID, userData, authData)
                        .then(function(id) {
                            deferred.resolve(id);
                        })
                        .catch(function(error) {
                            deferred.reject(error);
                        });
                });
            } else {
                endCreate(newID, userData, authData)
                    .then(function(id) {
                        deferred.resolve(id);
                    })
                    .catch(function(error) {
                        deferred.reject(error);
                    });
            }
            return deferred.promise;
        }

        function endCreate(newID, userData, authData) {
            var deferred = $q.defer();

            var id;
            if (authData.auth.fb_id) {
                id    = authData.auth.fb_id;
            } else {
                id    = authData.auth.uid;
            }

            var updates = {};
            updates["users/" + newID] = userData;
            //updates['users-verification/' + verificationID] = newID; // me.do if we go with verification email
            updates['users-email-uid/' + Base64.encode(userData.email)] = newID;
            updates['providers/' + id] = newID;

            ref.update(updates)
                .then(function() {
                    deferred.resolve(newID);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function UpdateRole(businessID, uID, newRole) {
            var deferred = $q.defer();

            var updates = {};
            updates["users/" + uID + "/businesses/" + businessID + "/role"] = newRole;

            ref.update(updates)
                .then(function() {
                    deferred.resolve("Entry has updated");
                })
                .catch(function() {
                    deferred.reject("Theres been an error");
                });

            return deferred.promise;
        }

        function GetAll() {
            return scope.data;
        }

        function GetByID(uid) {
            return new UserFactory(ref.child("users/" + uid));
        }

        function GetByEmail(email) {
            return ref.child('users-email-uid/' + Base64.encode(email));
        }
    }
})();