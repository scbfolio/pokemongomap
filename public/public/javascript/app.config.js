(function () {
    'use strict';

    angular
        .module('app')
        .config(configRouter)
        .config(forceSlach)
        .config(templateCacheBust)
    ;

    function configRouter($locationProvider, $urlMatcherFactoryProvider) {
        $locationProvider.html5Mode(true);
        $urlMatcherFactoryProvider.strictMode(false);

    }

    function forceSlach($urlRouterProvider) {
        // check to see if the path already has a slash where it should be
        $urlRouterProvider.rule(function ($injector, $location) {
            var path = $location.url();
            if (path[path.length - 1] === '/' || path.indexOf('/?') > -1) {return;}
            if (path.indexOf('?') > -1) {return path.replace('?', '/?');}

            return path + '/';
        });
    }

    function templateCacheBust($provide) {
        // Set a suffix outside the decorator function
        var cacheBuster = Date.now().toString();

        function templateFactoryDecorator($delegate) {
            var fromUrl = angular.bind($delegate, $delegate.fromUrl);

            $delegate.fromUrl = function (url, params) {
                if (url !== null && angular.isDefined(url) && angular.isString(url)) {
                    url += (url.indexOf("?") === -1 ? "?" : "&");
                    url += "v=" + cacheBuster;
                }

                return fromUrl(url, params);
            };

            return $delegate;
        }

        $provide.decorator('$templateFactory', ['$delegate', templateFactoryDecorator]);
    }

})();
