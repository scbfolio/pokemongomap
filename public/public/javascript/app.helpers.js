(function() {
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(obj, start) {
            for (var i = (start || 0), j = this.length; i < j; i++) {
                if (this[i] === obj) { return i; }
            }
            return -1;
        }
    }

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };

    // Avoid `console` errors in browsers that lack a console.
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }

    }
}());

function convertImgToDataURLviaCanvas(url, callback, outputFormat){
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function(){
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var dataURL;
        canvas.height = this.height;
        canvas.width = this.width;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        callback(dataURL);
        canvas = null;
    };
    img.src = url;
}

function getAverageRGB(imgEl) {
    var blockSize = 5, // only visit every 5 pixels
        defaultRGB = {r:0,g:0,b:0}, // for non-supporting envs
        canvas = document.createElement('canvas'),
        context = canvas.getContext && canvas.getContext('2d'),
        data, width, height,
        i = -4,
        length,
        rgb = {r:0,g:0,b:0},
        count = 0;

    if (!context) {
        return defaultRGB;
    }

    height = canvas.height = imgEl.naturalHeight || imgEl.offsetHeight || imgEl.height;
    width = canvas.width = imgEl.naturalWidth || imgEl.offsetWidth || imgEl.width;

    context.drawImage(imgEl, 0, 0);

    try {
        data = context.getImageData(0, 0, width, height);
    } catch(e) {
        /* security error, img on diff domain */alert('x');
        return defaultRGB;
    }

    length = data.data.length;

    while ( (i += blockSize * 4) < length ) {
        ++count;
        rgb.r += data.data[i];
        rgb.g += data.data[i+1];
        rgb.b += data.data[i+2];
    }

    // ~~ used to floor values
    rgb.r = ~~(rgb.r/count);
    rgb.g = ~~(rgb.g/count);
    rgb.b = ~~(rgb.b/count);

    return rgb;
}


(function(window, $) {
    var helpers = {};

    helpers.encodeURL = function(url) {
        return encodeURIComponent(url).replace(/'/g,"%27").replace(/"/g,"%22");
    };

    helpers.decodeURL = function(url) {
        return decodeURIComponent(url.replace(/\+/g,  " "));
    };

    helpers.setCookie = function(c_name, c_val, expiretime) {
        if (Modernizr.localstorage) {
            return localStorage.setItem(c_name, c_val);
        } else {
            return $.cookie(c_name, c_val, {expires: expiretime, path: '/'});
        }
    };

    helpers.getCookie = function(c_name) {
        if (Modernizr.localstorage) {
            return localStorage.getItem(c_name);
        } else {
            return $.cookie(c_name);
        }
    };

    helpers.removeCookie = function(c_name) {
        if (Modernizr.localstorage) {
            return localStorage.removeItem(c_name);
        } else {
            return $.removeCookie(c_name);
        }
    };

    helpers.hasCookie = function(c_name) {
        if (Modernizr.localstorage) {
            return (localStorage.getItem(c_name) != null) ? true : false;
        } else {
            return ($.cookie(c_name)) ? true : false;
        }
    };

    helpers.getFullUrl = function(withHash, withSearch) {
        var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
        if (withSearch) {url += window.location.search}
        if (withHash) {url += window.location.hash}
        return url;
    };

    helpers.toLowerUrl = function(name) {
        return name.replace(/([A-Z])/g, '-$1').toLowerCase();
    };

    helpers.toTemplateUrl = function(name) {
        return name.replace(/([A-Z])/g, '_$1').toLowerCase();
    };

    window.objectKeyToArrayValue = function(data) {
        var array = [];
        for(var index in data) {
            var currentData = data[index];
            currentData.key = index;
            array.push(currentData);
        }
        return array;
    };

    window.addEvt = (function () {
        if (document.addEventListener) {
            return function (el, type, fn) {
                if (el && el.nodeName || el === window) {
                    el.addEventListener(type, fn, false);
                } else if (el && el.length) {
                    for (var i = 0; i < el.length; i++) {
                        window.addEvt(el[i], type, fn);
                    }
                }
            };
        } else {
            return function (el, type, fn) {
                if (el && el.nodeName || el === window) {
                    el.attachEvent('on' + type, function () { return fn.call(el, window.event); });
                } else if (el && el.length) {
                    for (var i = 0; i < el.length; i++) {
                        window.addEvt(el[i], type, fn);
                    }
                }
            };
        }
    })();

    window.splitHash = function(href) {
        var data = {};
        var hashIndex = href.indexOf('#');
        if (hashIndex == -1) {
            data.url  = href;
            data.hash = null;
        } else {
            data.url  = href.substring(0, hashIndex);
            data.hash = href.substring(hashIndex);
        }
        return data;
    };

    window.sortByIndex = function compare(a,b) {
        if (a.index < b.index)
            return -1;
        else if (a.index > b.index)
            return 1;
        else
            return 0;
    };

    window.sortByPriority = function compare(a,b) {
        if (a.priority < b.priority)
            return -1;
        else if (a.priority > b.priority)
            return 1;
        else
            return 0;
    };

    window.Helpers = helpers;
}(window, jQuery));

(function($) {
    $.fn.exists = function(){if(jQuery(this).length == 0){return false;}else{return true;}};

    $.scrollWhereIWant = function($el, value, time, completeHandler) {
        var curScroll = Me.help.dimension.getScrollOffsets();
        if ($el != window) {curScroll = Me.help.dimension.getScrollOffsets($el);}
        var data  = {
            y:value,
            onUpdate:function() {
                if ($el == window) {$el.scrollTo(0, curScroll.y);}
                else {$el.scrollTop(curScroll.y);}
            }
        };
        if(typeof completeHandler === "function") {
            data.onComplete      = completeHandler;
            data.onCompleteScope = $el;
        }
        TweenLite.to(curScroll, time/1000, data);
    };
})(jQuery);
// END HELPERS