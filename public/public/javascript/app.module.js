(function() {
    'use strict';

    angular
        .module('app', [
            /* ng modules */
            'ngAnimate',
            'ngTouch',
            'ngSanitize',
            'ngMessages',

            /* ng external modules */
            'ui.router',
            'pascalprecht.translate',
            'anim-in-out',
            'ngMask',

            'firebase',

            'layout.auth',
            'layout.application'
        ]);
})();
