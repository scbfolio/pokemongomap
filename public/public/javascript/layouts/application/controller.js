(function() {
    'use strict';

    angular
        .module('layout.application')
        .controller('ApplicationCtrl', ApplicationCtrl);

    function ApplicationCtrl($rootScope, $scope, $element, serverTimeOffset) {
        var vm = this;
        vm.ctrlLoaded = false;

        init();

        function init() {
            $scope.$on('$destroy', destroy);
            $scope.$on('notification.init', function(e, data) {$scope.$broadcast('notification.inited', data);});
            $scope.$on('notification.enable', addNotficationClass);
            $scope.$on('notification.disable', removeNotficationClass);

            $scope.serverTimeOffset = serverTimeOffset;
        }

        function addNotficationClass() {
            $element.addClass('notification-active');
        }

        function removeNotficationClass() {
            $element.removeClass('notification-active');
        }

        function destroy() {
        }
    }
})();
