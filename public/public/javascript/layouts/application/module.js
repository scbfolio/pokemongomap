(function() {
    'use strict';

    angular
        .module('layout.application', [
            'map',
        ]);
})();
