(function() {
    'use strict';

    angular
        .module('layout.application')
        .config(Route);

    function Route($stateProvider) {
        $stateProvider.state('app.application', {
            abstract: true,
            url: '',
            views : {
                '' : {
                    templateUrl : 'public/javascript/layouts/application/index.html',
                    controller : 'ApplicationCtrl',
                    controllerAs : 'vm'
                }
            },
            resolve: {
                "waitForAuth": function($rootScope, $q, AuthenticationService) {
                    var deferred = $q.defer();
                    deferred.resolve();
                   /* AuthenticationService.GetAuth().$requireAuth()
                        .then(function() {
                            authLoading();
                        })
                        .catch(function() {
                            deferred.reject("AUTH_REQUIRED");
                        });

                    function authLoading() {
                        if ($rootScope.globals.authLoaded) {deferred.resolve(); return;}

                        setTimeout(authLoading, 10);
                    }*/

                    return deferred.promise;
                },
                "serverTimeOffset": function($rootScope, $q, $firebaseObject, waitForAuth) {
                    var deferred = $q.defer();

                    var serverTimeOffset = $firebaseObject(SETTINGS.FIREBASE_REF.child('.info/serverTimeOffset'));
                    serverTimeOffset.$loaded()
                        .then(function() {
                            console.log(new Date(new Date().getTime() + serverTimeOffset.$value));
                            $rootScope.serverTimeOffset = serverTimeOffset.$value;
                            deferred.resolve(serverTimeOffset.$value);
                        });

                    return deferred.promise;
                }
            }
        });
    }
})();
