(function() {
    'use strict';

    angular
        .module('layout.auth', [
            'login',
            'register',
            'forgotPassword'
        ]);
})();
