(function() {
    'use strict';

    angular
        .module('layout.auth')
        .controller('AuthCtrl', AuthCtrl);

    function AuthCtrl($rootScope, $scope) {
        var vm = this;
        vm.ctrlLoaded = true;

        $rootScope.showAuthError = showAuthError;

        init();

        function init() {

        }

        function showAuthError(ERROR_CODE) {
            $scope.$broadcast('show_error', ERROR_CODE);
        }
    }
})();
