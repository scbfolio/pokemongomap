(function() {
    'use strict';

    angular
        .module('layout.auth')
        .config(Route);

    function Route($stateProvider) {
        $stateProvider.state('app.auth', {
            abstract: true,
            url: '',
            views : {
                '' : {
                    templateUrl : 'public/javascript/layouts/auth/index.html',
                    controller : 'AuthCtrl',
                    controllerAs : 'vm'
                }
            },
            resolve: {
                "NoAuthRequired": function($q, AuthenticationService) {
                    var deferred = $q.defer();

                    AuthenticationService.GetAuth().$waitForAuth()
                        .then(function(auth) {
                            if (auth) {
                                deferred.reject("ALREADY_CONNECTED");
                            } else {
                                deferred.resolve();
                            }
                        });

                    return deferred.promise;
                }
            }
        });
    }
})();
