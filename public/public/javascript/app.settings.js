var SETTINGS = {
    APP_VERSION: 1,
    LANGUAGE: "en",
    BASE_URL: window.location.protocol + "//" + window.location.host + "/",
    CURRENT_URL: window.location.protocol + "//" + window.location.host + window.location.pathname,
    CDN_URL: "", // if needed
    VERSIONNING: new Date().getTime(), //for caching
    DEVICE: "computer", // if needed
    IS_MOBILE: false, // if needed
    IS_TABLET: false, // if needed
    DEBUG_MODE: true // turn false if you wanna disable console.
};

SETTINGS.LANGUAGES = [
    {
        code:'en',
        term: 'English'
    },
    {
        code:'fr',
        term: 'Francais'
    }
];

SETTINGS.ISIOS = /iPhone|iPad|iPod/i.test(navigator.userAgent);