(function() {
    'use strict';

    var moduleName = "map";

    angular
        .module(moduleName)
        .config(Route);

    function Route($stateProvider) {
        $stateProvider.state('app.application.' + moduleName, {
            url: 'map/',
            views: {
                '': {
                    templateUrl: Helpers.toLowerUrl('public/javascript/interfaces/application/map/index.html'),
                    controller: moduleName.capitalize() + 'Ctrl',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        });
    }
})();