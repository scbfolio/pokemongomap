(function () {
    'use strict';

    angular
        .module('map')
        .controller('MapCtrl', MapCtrl);

    function MapCtrl($rootScope, $scope, $element, $firebaseArray, $firebaseObject) {
        var vm        = this;
        vm.ctrlLoaded = false;
        vm.data       = {};

        vm.geoSuccess    = geoSuccess;
        vm.geoError      = geoError;
        vm.submit        = submit;
        vm.preparePinAdd = preparePinAdd;
        vm.loadNewImage  = loadNewImage;
        vm.addMarker     = addMarker;
        vm.removeMarker  = removeMarker;
        vm.destroy       = destroy;
        init();

        function init() {
            vm.ctrlLoaded        = true;
            vm.pokemons          = window.pokemon_data.pokemon.slice(1, 152);
            vm.googleMapInstance = new google.maps.Map($element.find('#map')[0], {
                center: {lat: -34.397, lng: 150.644},
                zoom: 17,
                maxZoom: 18,
                minZoom: 10
            });


            var image = {
                url: "http://www.cpokemon.com/wp-content/uploads/2016/01/cpokemon.com_app_icon.png",
                scaledSize: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(15, 30)
            };

            vm.infoWindow = new google.maps.Marker({
                map: vm.googleMapInstance,
                icon: image,
                zIndex: 999
            });

            vm.markers       = {};
            vm.markers_index = 0;
            vm.pins          = $firebaseArray(firebase.database().ref().child('pins'));
            vm.pins.$watch(function(data) {
                if (data.event == "child_added" || data.event == "child_changed") {
                    vm.addMarker(data.key);
                } else if (data.event == "child_removed") {
                    vm.removeMarker(data.key);
                }
            });


            if (navigator.geolocation) {
                var options = {
                    enableHighAccuracy: true,
                    timeout: 5000,
                    maximumAge: 0
                };
                navigator.geolocation.watchPosition(vm.geoSuccess, vm.geoError, options);
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, vm.googleMapInstance.getCenter());
            }

            $scope.$on('$destroy', destroy);
        }

        function geoSuccess(pos) {
            var crd = pos.coords;
            var my_position = {
                lat: crd.latitude,
                lng: crd.longitude
            };

            vm.infoWindow.setPosition(my_position);
            vm.googleMapInstance.setCenter(my_position);
        }

        function geoError(err) {
            console.warn('ERROR(' + err.code + '): ' + err.message);
            handleLocationError(true, vm.googleMapInstance.getCenter());
        }

        function submit() {
            var newPinID = firebase.database().ref().child('pins').push().key;
            var data     = {};
            var updates  = {};

            if (vm.data.type == "gym") {
                data.type     = vm.data.type;
                data.position = vm.data.pinLatLng;
                data.ref_id   = firebase.database().ref().child('gym').push().key;
                updates['gym/' + data.ref_id] = {
                    name: vm.data.name
                };
            } else if (vm.data.type == "pokestop") {
                data.type     = vm.data.type;
                data.position = vm.data.pinLatLng;
                data.ref_id   = firebase.database().ref().child('pokestop').push().key;
                updates['pokestop/' + data.ref_id] = {
                    name: vm.data.name
                };
            } else if (vm.data.type == "pokemon") {
                data.type     = vm.data.type;
                data.position = vm.data.pinLatLng;
                data.ref_id   = firebase.database().ref().child('pokemon').push().key;
                updates['pokemon/' + data.ref_id] = {
                    pokemon_index: Number(vm.data.pokemonID)
                };
            }

            data.added = firebase.database.ServerValue.TIMESTAMP;

            updates['/pins/' + newPinID] = data;
            firebase.database().ref().update(updates)
                .then(function() {
                    vm.addModalOpen = false;
                    vm.marker.setMap(null);
                    vm.circle.setMap(null);
                    vm.marker = null;
                    vm.circle = null;
                    vm.data.pokemonID = null;
                });
        }

        function preparePinAdd() {
            vm.addModalOpen   = false;
            vm.data.type      = 'gym';
            vm.data.pokemonID = null;
            if (vm.marker) {vm.marker.setMap(null);}
            if (vm.circle) {vm.circle.setMap(null);}

            vm.circle = new google.maps.Circle({
                map: vm.googleMapInstance,
                radius: 250,
                fillColor: '#AA0000',
                clickable: true,
                index:0
            });
            vm.circle.bindTo('center',  vm.infoWindow, 'position');

            vm.mapClickEvent = google.maps.event.addListener(vm.circle, 'click', function(event) {
                vm.data.pinLatLng = {lat:event.latLng.lat(), lng:event.latLng.lng()};

                vm.marker = new google.maps.Marker({
                    position: vm.data.pinLatLng,
                    map: vm.googleMapInstance,
                    zIndex: 999
                });

                vm.addModalOpen = true;
                google.maps.event.removeListener(vm.mapClickEvent);

                $scope.$apply();
            });
        }

        function redraw() {
            vm.googleMapInstance.setCenter({lat: lat, lng : lng, alt: 0});
            map_marker.setPosition({lat: lat, lng : lng, alt: 0});
        }

        function loadNewImage() {
            if (!vm.data.pokemonID) {
                vm.pokemon_img = null;
            } else {
                vm.pokemon_img = 'http://assets.pokemon.com/assets/cms2/img/pokedex/full/' + lpad(vm.pokemons[vm.data.pokemonID].id, 3) + '.png';
            }

        }

        function addMarker(key) {
            var data   = getEntryByID(vm.pins, key).data;
            data.data = $firebaseObject(firebase.database().ref().child(data.type + "/" + data.ref_id));
            data.data.$loaded()
                .then(function() {
                    if (!vm.markers[key]) {
                        var img_src      = null;
                        var img_size     = {width:0, height:0};
                        var scaled_size  = {width:30, height:30};
                        var origin_point = {width:0, height:0};

                        if (data.type == "gym") {
                            img_src = "https://cdn1.iconfinder.com/data/icons/hotel-restaurant/512/3-512.png";
                            img_size.width  = 512;
                            img_size.heigth = 512;
                        } else if (data.type == "pokestop") {
                            img_src = "https://cdn4.iconfinder.com/data/icons/business-soft/512/market_seller-512.png";
                            img_size.width  = 512;
                            img_size.heigth = 512;
                        } else if (data.type == "pokemon") {
                            console.log(data);
                            img_src = "http://assets.pokemon.com/assets/cms2/img/pokedex/full/" + lpad(vm.pokemons[Number(data.data.pokemon_index)].id, 3) + ".png";
                            img_size.width  = 300;
                            img_size.heigth = 300;
                        }
                        var image = {
                            url: img_src,
                            scaledSize: new google.maps.Size(scaled_size.width, scaled_size.height),
                            origin: new google.maps.Point(origin_point.width, origin_point.height),
                            anchor: new google.maps.Point(15, 30)
                        };

                        var marker = new google.maps.Marker({
                            map: vm.googleMapInstance,
                            icon: image,
                            zIndex: vm.markers_index
                        });

                        vm.markers_index ++;
                        vm.markers[key] = marker;
                    }

                    // update position
                    var newLatLng = new google.maps.LatLng(data.position.lat, data.position.lng);
                    vm.markers[key].setPosition(newLatLng);
                });
        }

        function removeMarker(key) {
            vm.markers[key].setMap(null);
            delete vm.markers[key];
        }

        function handleLocationError(browserHasGeolocation, pos) {
            vm.infoWindow.setPosition(pos);
            console.log(browserHasGeolocation ? 'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.');
        }

        function destroy() {
        }

        function lpad(value, padding) {
            var zeroes = new Array(padding+1).join("0");
            return (zeroes + value).slice(-padding);
        }

        function getEntryByID(array, ID) {
            var chosenItem = null;
            $.each(array, function(index, value) {
                if (value.$id == ID) {chosenItem = {key:index, data:value}; return false;}
            });
            return chosenItem;
        }
    }
})();
