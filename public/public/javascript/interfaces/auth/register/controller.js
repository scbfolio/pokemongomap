(function() {
    'use strict';

    angular
        .module('register')
        .controller('RegisterCtrl', RegisterCtrl);

    function RegisterCtrl($rootScope, $scope, $element, $state, $translate, AuthenticationService, UsersService, auth) {
        var vm          = this;
        vm.ctrlLoaded   = true;
        vm.data         = {};
        vm.isSubmitting = false;
        vm.errorMessages = [];
        vm.successMessages = [];
        vm.errorMessagesSocial = [];

        vm.submit          = submit;
        vm.logWithLinkedIn = logWithLinkedIn;
        vm.logWithGoogle   = logWithGoogle;
        vm.displayErrors   = displayErrors;
        vm.destroy         = destroy;

        init();

        function init() {
            vm.messages = {};
            $translate([
                'auth.sign_up.errors.user_email_address',
                'auth.sign_up.success',
                'auth.login.errors.google_error',
                'auth.login.errors.linkedin_error'
            ]).then(function (translations) {
                vm.messages = translations;
            });

            $scope.$on('$destroy', destroy);
        }

        function submit() {
            if (vm.isSubmitting || $scope.register.$invalid) {return;}

            vm.isSubmitting = true;
            vm.errorMessages = [];

            AuthenticationService.Register(vm.data)
                .then(function(authData) {
                    vm.successMessages.push(vm.messages['auth.sign_up.success']);
                    $rootScope.RegisterData = vm.data;
                })
                .catch(function(error) {
                    vm.errorMessages.push(vm.messages['auth.sign_up.errors.user_email_address']);
                    vm.isSubmitting = false;
                });
        }

        function logWithLinkedIn() {
            auth.signin({
                popup: true,
                connection: 'linkedin',
                scope: 'openid name email picture',
            }, function(profile) {
                $rootScope.stockedUserProfile = profile;
                auth.getToken({api: 'firebase', scope: 'openid name email picture',})
                    .then(function(delegation) {
                        AuthenticationService.LogWithLinkedIn(delegation.id_token)
                            .then(function(authData) {

                            })
                            .catch(function(error) {
                                vm.errorMessagesSocial = [];
                                vm.errorMessagesSocial.push(vm.messages['auth.login.errors.linkedin_error']);
                                vm.isSubmitting = false;
                            });
                    })
                    .catch(function(error) {
                        vm.errorMessagesSocial = [];
                        vm.errorMessagesSocial.push(vm.messages['auth.login.errors.linkedin_error']);
                        vm.isSubmitting = false;
                    });
            }, function(error) {
                vm.errorMessagesSocial = [];
                vm.errorMessagesSocial.push(vm.messages['auth.login.errors.linkedin_error']);
                vm.isSubmitting = false;
            });
        }

        function logWithGoogle() {
            AuthenticationService.LogWithGoogle()
                .then(function(authData) {

                })
                .catch(function(error) {
                    vm.errorMessagesSocial = [];
                    vm.errorMessagesSocial.push(vm.messages['auth.login.errors.google_error']);
                    vm.isSubmitting = false;
                });
        }

        function displayErrors(e, data) {
            vm.errorMessagesSocial = [];
            vm.errorMessagesSocial.push(vm.messages['auth.sign_up.errors.user_email_address']);
        }

        function destroy() {
        }
    }
})();
