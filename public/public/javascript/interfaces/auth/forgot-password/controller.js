(function() {
    'use strict';

    angular
        .module('forgotPassword')
        .controller('ForgotPasswordCtrl', ForgotPasswordCtrl);

    function ForgotPasswordCtrl($rootScope, $scope, $element, $state, $timeout, $translate, AuthenticationService) {
        var vm        = this;
        vm.ctrlLoaded = false;
        vm.data         = {};
        vm.isSubmitting = false;
        vm.errorMessages = [];
        vm.successMessages = [];

        vm.submit     = submit;
        vm.destroy    = destroy;

        init();

        function init() {
            vm.messages = {};
            $translate(['auth.forgot_password.errors.invalid_user', 'auth.forgot_password.success']).then(function (translations) {
                vm.messages = translations;
            });

            $scope.$on('$destroy', destroy);
        }

        function submit() {
            if (vm.isSubmitting || $scope.forgot_password.$invalid) {
                $scope.forgot_password.$setSubmitted();
                return;
            }

            vm.isSubmitting = true;
            vm.errorMessages = [];

            AuthenticationService.ForgotPassword(vm.data)
                .then(function() {
                    vm.succeed = true;
                    vm.successMessages.push(vm.messages['auth.forgot_password.success']);
                    vm.isSubmitting = false;

                    $timeout(function() {
                        //$state.go('app.auth.login');
                    }, 5000)
                })
                .catch(function(error) {
                    vm.errorMessages.push(vm.messages['auth.forgot_password.errors.invalid_user']);
                    vm.isSubmitting = false;
                });
        }

        function destroy() {
        }
    }
})();
