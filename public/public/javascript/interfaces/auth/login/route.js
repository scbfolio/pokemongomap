(function() {
    'use strict';

    var moduleName = "login";

    angular
        .module(moduleName)
        .config(Route);

    function Route($stateProvider) {
        $stateProvider.state('app.auth.' + moduleName, {
            url: Helpers.toLowerUrl(moduleName + '/'),
            views : {
                '' : {
                    templateUrl  : Helpers.toLowerUrl('public/javascript/interfaces/auth/' + moduleName + '/index.html'),
                    controller   : moduleName.capitalize() + 'Ctrl',
                    controllerAs : 'vm'
                }
            }
        });
    }
})();