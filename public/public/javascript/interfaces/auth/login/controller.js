(function() {
    'use strict';

    angular
        .module('login')
        .controller('LoginCtrl', LoginCtrl);

    function LoginCtrl($rootScope, $scope, $element, $state, $translate, AuthenticationService, UsersService) {
        var vm          = this;
        vm.ctrlLoaded   = true;
        vm.data         = {};
        vm.isSubmitting = false;
        vm.errorMessages = [];
        vm.errorMessagesSocial = [];

        vm.submit          = submit;
        vm.logWithGoogle   = logWithGoogle;
        vm.displayErrors   = displayErrors;
        vm.destroy         = destroy;

        init();

        function init() {
            vm.errors = {};
            $translate([
                'auth.sign_up.errors.user_email_address',
                'auth.login.errors.invalid_login',
                'auth.login.errors.google_error',
                'auth.login.errors.linkedin_error'
            ]).then(function(translations) {
                vm.errors = translations;
            });

            $scope.$on('show_error', displayErrors);
            $scope.$on('$destroy', destroy);
        }

        function submit() {
            vm.validated = true;

            if (vm.isSubmitting || $scope.login.$invalid) {return;}

            vm.isSubmitting  = true;
            vm.errorMessages = [];

            AuthenticationService.Login(vm.data)
                .then(function(authData) {
                    vm.isSubmitting = false;
                })
                .catch(function(error) {
                    vm.errorMessages.push(vm.errors['auth.login.errors.invalid_login']);
                    vm.isSubmitting = false;
                });
        }

        function logWithGoogle() {
            AuthenticationService.LogWithGoogle()
                .then(function(authData) {

                })
                .catch(function(error) {
                    vm.errorMessagesSocial = [];
                    vm.errorMessagesSocial.push(vm.errors['auth.login.errors.google_error']);
                    vm.isSubmitting = false;
                });
        }

        function displayErrors(e, data) {
            vm.errorMessagesSocial = [];
            vm.errorMessagesSocial.push(vm.errors['auth.sign_up.errors.user_email_address']);
        }

        function destroy() {
        }
    }
})();