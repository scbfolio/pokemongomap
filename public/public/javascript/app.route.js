(function() {
    'use strict';

    angular
        .module('app')
        .config(setDefaultRoute)
        .config(Route)
        .run(CoreRouteRun);

    function setDefaultRoute($urlRouterProvider) {
        $urlRouterProvider.otherwise(function($injector) {
            var $state = $injector.get("$state");
            $state.go('app.application.map');
        });
    }

    function Route($stateProvider) {
        $stateProvider.state('app', {
            abstract:true,
            url: "/",
            template: '<div class="full-page" ui-view></div>'
        });
    }

    function CoreRouteRun($rootScope, $state) {}
})();
