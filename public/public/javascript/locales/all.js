(function() {
    'use strict';
    angular
        .module('app')
        .config(configurateLocales);

    function configurateLocales($translateProvider) {
        $translateProvider.useSanitizeValueStrategy(null);
        $translateProvider.fallbackLanguage(SETTINGS.LANGUAGE);
    }
})();