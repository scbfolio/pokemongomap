(function() {
    'use strict';

    angular
        .module('app')
        .config(addLanguage);

    var translations = {
        roles: {
            0:  "Guest",
            10: "User",
            19: "Admin",
            20: "Administrator", /* UPDATED */
            97: "Administrator",
            98: "Super Administrator",
            99: "Node Server"
        },
        presence: {
            0: "Offline",
            1: "Away",
            2: "Online"
        },
        status: {
            pending:   "Pending",
            blocked:   "Blocked",
            completed: "Completed",
            opened:    "Opened"
        },
        loading: "Loading...",
        time_format: "hh:mm A",
        ui:{
            button_back: "Back",
            button_cancel: "Cancel",
            button_sign_in: "Sign in",
            button_sign_up: "Sign up",
            button_save: "Save",
            button_remove: "Remove",
            button_confirm: "Confirm",
            button_file_input: "Upload",
            button_submit: "Submit",
            button_add_company: "Add a new company",
            button_delete_business: "Delete the business",
            button_google: "With Google",
            button_linkedin: "With LinkedIn",
            button_create_from_linkedin: "Create from LinkedIn profile",
            button_enter_manually: "Enter manually",
            button_start_strategic_planning: "Start my strategic planning",
            button_join_strategic_planning: "Join current strategic planning",
            label_file_input: "Company Logo",
            select_no_match: "No match found" /* TODO ADDED */
        },
        actions: {
            ok: "Ok",
            yes: "Yes",
            add: "Add",
            edit: "Edit",
            delete: "Delete",
            reassign: "Reassign",
            save: "Save",
            push_to_pending: "Push to pending",
            push_to_opened: "Push to opened",
            push_to_completed: "Push to completed",
            push_to_final_cut: "Push to final cut",
            push_to_candidates: "Push to candidates",
            manage_your_team: "Manage your team",
            invite_someone: "Invite someone",
            remove: "Remove",
            cancel: "Cancel",
            upgrade_now: "Upgrade now",
            next_step: "Next step",
            catch_up: "Catch up"
        },
        errors: {
            default: "An error has occured. Please try again.",
            required: "The field is required.",
            valid_email: "Please enter a valid email address.",
            valid_password: "Your password must contains at least one uppercase, one lowercase, one special character or a number and be 8 characters long."
        },
        auth: {
            login:{
                title: "Sign in",
                or: "or",
                forgot_password: "Forgot your password?",
                no_account: "Don't have an account?",
                placeholders: {
                    email: "Your email address",
                    password: "Password"
                },
                errors: {
                    invalid_login: 'The username or the password is invalid.',
                    google_error: 'An error has occured while trying to log you in with google. Make sure you accepted the required permissions.',
                    linkedin_error: 'An error has occured while trying to log you in with linkedin. Make sure you accepted the required permissions.'
                }
            },
            sign_up:{
                title: "Sign up",
                or: "or",
                have_account: "Have an account?",
                placeholders:{
                    first_name: "First Name",
                    last_name: "Last Name",
                    email: "Email",
                    password: "Choose a password",
                    confirm_password: "Confirm your password"
                },
                success: "Your account was created with success. You will now be redirected.",
                errors: {
                    user_email_address: "This email address is already associated with another account."
                }
            },
            forgot_password:{
                title: "Password retrieval",
                back_to_login: "Go back to login",
                success: "Check your email you should have received indications to reset your password.",
                errors: {
                    invalid_user: "There is no valid user associated to this email address."
                }
            }
        },
    };

    function addLanguage($translateProvider) {
        $translateProvider.translations('en', translations);
    }
})();