window.CURRENCIES = {
    "AED": {
        "name": "UAE Dirham"
    },
    "AFN": {
        "name": "Afghani",
        "symbol": "؋"
    },
    "ALL": {
        "name": "Lek",
        "symbol": "Lek"
    },
    "AMD": {
        "name": "Armenian Dram"
    },
    "ANG": {
        "name": "Netherlands Antillian Guilder",
        "symbol": "ƒ"
    },
    "AOA": {
        "name": "Kwanza"
    },
    "ARS": {
        "name": "Argentine Peso",
        "symbol": "$"
    },
    "AUD": {
        "name": "Australian Dollar",
        "symbol": "$"
    },
    "AWG": {
        "name": "Aruban Guilder",
        "symbol": "ƒ"
    },
    "AZN": {
        "name": "Azerbaijanian Manat",
        "symbol": "ман"
    },
    "BAM": {
        "name": "Convertible Marks",
        "symbol": "KM"
    },
    "BBD": {
        "name": "Barbados Dollar",
        "symbol": "$"
    },
    "BDT": {
        "name": "Taka"
    },
    "BGN": {
        "name": "Bulgarian Lev",
        "symbol": "лв"
    },
    "BHD": {
        "name": "Bahraini Dinar"
    },
    "BIF": {
        "name": "Burundi Franc"
    },
    "BMD": {
        "name": "Bermudian Dollar (customarily known as Bermuda Dollar)",
        "symbol": "$"
    },
    "BND": {
        "name": "Brunei Dollar",
        "symbol": "$"
    },
    "BOB BOV": {
        "name": "Boliviano Mvdol",
        "symbol": "$b"
    },
    "BRL": {
        "name": "Brazilian Real",
        "symbol": "R$"
    },
    "BSD": {
        "name": "Bahamian Dollar",
        "symbol": "$"
    },
    "BWP": {
        "name": "Pula",
        "symbol": "P"
    },
    "BYR": {
        "name": "Belarussian Ruble",
        "symbol": "p."
    },
    "BZD": {
        "name": "Belize Dollar",
        "symbol": "BZ$"
    },
    "CAD": {
        "name": "Canadian Dollar",
        "symbol": "$"
    },
    "CDF": {
        "name": "Congolese Franc"
    },
    "CHF": {
        "name": "Swiss Franc",
        "symbol": "CHF"
    },
    "CLP CLF": {
        "name": "Chilean Peso Unidades de fomento",
        "symbol": "$"
    },
    "CNY": {
        "name": "Yuan Renminbi",
        "symbol": "¥"
    },
    "COP COU": {
        "name": "Colombian Peso Unidad de Valor Real",
        "symbol": "$"
    },
    "CRC": {
        "name": "Costa Rican Colon",
        "symbol": "₡"
    },
    "CUP CUC": {
        "name": "Cuban Peso Peso Convertible",
        "symbol": "₱"
    },
    "CVE": {
        "name": "Cape Verde Escudo"
    },
    "CZK": {
        "name": "Czech Koruna",
        "symbol": "Kč"
    },
    "DJF": {
        "name": "Djibouti Franc"
    },
    "DKK": {
        "name": "Danish Krone",
        "symbol": "kr"
    },
    "DOP": {
        "name": "Dominican Peso",
        "symbol": "RD$"
    },
    "DZD": {
        "name": "Algerian Dinar"
    },
    "EEK": {
        "name": "Kroon"
    },
    "EGP": {
        "name": "Egyptian Pound",
        "symbol": "£"
    },
    "ERN": {
        "name": "Nakfa"
    },
    "ETB": {
        "name": "Ethiopian Birr"
    },
    "EUR": {
        "name": "Euro",
        "symbol": "€"
    },
    "FJD": {
        "name": "Fiji Dollar",
        "symbol": "$"
    },
    "FKP": {
        "name": "Falkland Islands Pound",
        "symbol": "£"
    },
    "GBP": {
        "name": "Pound Sterling",
        "symbol": "£"
    },
    "GEL": {
        "name": "Lari"
    },
    "GHS": {
        "name": "Cedi"
    },
    "GIP": {
        "name": "Gibraltar Pound",
        "symbol": "£"
    },
    "GMD": {
        "name": "Dalasi"
    },
    "GNF": {
        "name": "Guinea Franc"
    },
    "GTQ": {
        "name": "Quetzal",
        "symbol": "Q"
    },
    "GYD": {
        "name": "Guyana Dollar",
        "symbol": "$"
    },
    "HKD": {
        "name": "Hong Kong Dollar",
        "symbol": "$"
    },
    "HNL": {
        "name": "Lempira",
        "symbol": "L"
    },
    "HRK": {
        "name": "Croatian Kuna",
        "symbol": "kn"
    },
    "HTG USD": {
        "name": "Gourde US Dollar"
    },
    "HUF": {
        "name": "Forint",
        "symbol": "Ft"
    },
    "IDR": {
        "name": "Rupiah",
        "symbol": "Rp"
    },
    "ILS": {
        "name": "New Israeli Sheqel",
        "symbol": "₪"
    },
    "INR": {
        "name": "Indian Rupee"
    },
    "INR BTN": {
        "name": "Indian Rupee Ngultrum"
    },
    "IQD": {
        "name": "Iraqi Dinar"
    },
    "IRR": {
        "name": "Iranian Rial",
        "symbol": "﷼"
    },
    "ISK": {
        "name": "Iceland Krona",
        "symbol": "kr"
    },
    "JMD": {
        "name": "Jamaican Dollar",
        "symbol": "J$"
    },
    "JOD": {
        "name": "Jordanian Dinar"
    },
    "JPY": {
        "name": "Yen",
        "symbol": "¥"
    },
    "KES": {
        "name": "Kenyan Shilling"
    },
    "KGS": {
        "name": "Som",
        "symbol": "лв"
    },
    "KHR": {
        "name": "Riel",
        "symbol": "៛"
    },
    "KMF": {
        "name": "Comoro Franc"
    },
    "KPW": {
        "name": "North Korean Won",
        "symbol": "₩"
    },
    "KRW": {
        "name": "Won",
        "symbol": "₩"
    },
    "KWD": {
        "name": "Kuwaiti Dinar"
    },
    "KYD": {
        "name": "Cayman Islands Dollar",
        "symbol": "$"
    },
    "KZT": {
        "name": "Tenge",
        "symbol": "лв"
    },
    "LAK": {
        "name": "Kip",
        "symbol": "₭"
    },
    "LBP": {
        "name": "Lebanese Pound",
        "symbol": "£"
    },
    "LKR": {
        "name": "Sri Lanka Rupee",
        "symbol": "₨"
    },
    "LRD": {
        "name": "Liberian Dollar",
        "symbol": "$"
    },
    "LTL": {
        "name": "Lithuanian Litas",
        "symbol": "Lt"
    },
    "LVL": {
        "name": "Latvian Lats",
        "symbol": "Ls"
    },
    "LYD": {
        "name": "Libyan Dinar"
    },
    "MAD": {
        "name": "Moroccan Dirham"
    },
    "MDL": {
        "name": "Moldovan Leu"
    },
    "MGA": {
        "name": "Malagasy Ariary"
    },
    "MKD": {
        "name": "Denar",
        "symbol": "ден"
    },
    "MMK": {
        "name": "Kyat"
    },
    "MNT": {
        "name": "Tugrik",
        "symbol": "₮"
    },
    "MOP": {
        "name": "Pataca"
    },
    "MRO": {
        "name": "Ouguiya"
    },
    "MUR": {
        "name": "Mauritius Rupee",
        "symbol": "₨"
    },
    "MVR": {
        "name": "Rufiyaa"
    },
    "MWK": {
        "name": "Kwacha"
    },
    "MXN MXV": {
        "name": "Mexican Peso Mexican Unidad de Inversion (UDI)",
        "symbol": "$"
    },
    "MYR": {
        "name": "Malaysian Ringgit",
        "symbol": "RM"
    },
    "MZN": {
        "name": "Metical",
        "symbol": "MT"
    },
    "NGN": {
        "name": "Naira",
        "symbol": "₦"
    },
    "NIO": {
        "name": "Cordoba Oro",
        "symbol": "C$"
    },
    "NOK": {
        "name": "Norwegian Krone",
        "symbol": "kr"
    },
    "NPR": {
        "name": "Nepalese Rupee",
        "symbol": "₨"
    },
    "NZD": {
        "name": "New Zealand Dollar",
        "symbol": "$"
    },
    "OMR": {
        "name": "Rial Omani",
        "symbol": "﷼"
    },
    "PAB USD": {
        "name": "Balboa US Dollar",
        "symbol": "B/."
    },
    "PEN": {
        "name": "Nuevo Sol",
        "symbol": "S/."
    },
    "PGK": {
        "name": "Kina"
    },
    "PHP": {
        "name": "Philippine Peso",
        "symbol": "Php"
    },
    "PKR": {
        "name": "Pakistan Rupee",
        "symbol": "₨"
    },
    "PLN": {
        "name": "Zloty",
        "symbol": "zł"
    },
    "PYG": {
        "name": "Guarani",
        "symbol": "Gs"
    },
    "QAR": {
        "name": "Qatari Rial",
        "symbol": "﷼"
    },
    "RON": {
        "name": "New Leu",
        "symbol": "lei"
    },
    "RSD": {
        "name": "Serbian Dinar",
        "symbol": "Дин."
    },
    "RUB": {
        "name": "Russian Ruble",
        "symbol": "руб"
    },
    "RWF": {
        "name": "Rwanda Franc"
    },
    "SAR": {
        "name": "Saudi Riyal",
        "symbol": "﷼"
    },
    "SBD": {
        "name": "Solomon Islands Dollar",
        "symbol": "$"
    },
    "SCR": {
        "name": "Seychelles Rupee",
        "symbol": "₨"
    },
    "SDG": {
        "name": "Sudanese Pound"
    },
    "SEK": {
        "name": "Swedish Krona",
        "symbol": "kr"
    },
    "SGD": {
        "name": "Singapore Dollar",
        "symbol": "$"
    },
    "SHP": {
        "name": "Saint Helena Pound",
        "symbol": "£"
    },
    "SLL": {
        "name": "Leone"
    },
    "SOS": {
        "name": "Somali Shilling",
        "symbol": "S"
    },
    "SRD": {
        "name": "Surinam Dollar",
        "symbol": "$"
    },
    "STD": {
        "name": "Dobra"
    },
    "SVC USD": {
        "name": "El Salvador Colon US Dollar",
        "symbol": "$"
    },
    "SYP": {
        "name": "Syrian Pound",
        "symbol": "£"
    },
    "SZL": {
        "name": "Lilangeni"
    },
    "THB": {
        "name": "Baht",
        "symbol": "฿"
    },
    "TJS": {
        "name": "Somoni"
    },
    "TMT": {
        "name": "Manat"
    },
    "TND": {
        "name": "Tunisian Dinar"
    },
    "TOP": {
        "name": "Pa'anga"
    },
    "TRY": {
        "name": "Turkish Lira",
        "symbol": "TL"
    },
    "TTD": {
        "name": "Trinidad and Tobago Dollar",
        "symbol": "TT$"
    },
    "TWD": {
        "name": "New Taiwan Dollar",
        "symbol": "NT$"
    },
    "TZS": {
        "name": "Tanzanian Shilling"
    },
    "UAH": {
        "name": "Hryvnia",
        "symbol": "₴"
    },
    "UGX": {
        "name": "Uganda Shilling"
    },
    "USD": {
        "name": "US Dollar",
        "symbol": "$"
    },
    "UYU UYI": {
        "name": "Peso Uruguayo Uruguay Peso en Unidades Indexadas",
        "symbol": "$U"
    },
    "UZS": {
        "name": "Uzbekistan Sum",
        "symbol": "лв"
    },
    "VEF": {
        "name": "Bolivar Fuerte",
        "symbol": "Bs"
    },
    "VND": {
        "name": "Dong",
        "symbol": "₫"
    },
    "VUV": {
        "name": "Vatu"
    },
    "WST": {
        "name": "Tala"
    },
    "XAF": {
        "name": "CFA Franc BEAC"
    },
    "XAG": {
        "name": "Silver"
    },
    "XAU": {
        "name": "Gold"
    },
    "XBA": {
        "name": "Bond Markets Units European Composite Unit (EURCO)"
    },
    "XBB": {
        "name": "European Monetary Unit (E.M.U.-6)"
    },
    "XBC": {
        "name": "European Unit of Account 9(E.U.A.-9)"
    },
    "XBD": {
        "name": "European Unit of Account 17(E.U.A.-17)"
    },
    "XCD": {
        "name": "East Caribbean Dollar",
        "symbol": "$"
    },
    "XDR": {
        "name": "SDR"
    },
    "XFU": {
        "name": "UIC-Franc"
    },
    "XOF": {
        "name": "CFA Franc BCEAO"
    },
    "XPD": {
        "name": "Palladium"
    },
    "XPF": {
        "name": "CFP Franc"
    },
    "XPT": {
        "name": "Platinum"
    },
    "XTS": {
        "name": "Codes specifically reserved for testing purposes"
    },
    "YER": {
        "name": "Yemeni Rial",
        "symbol": "﷼"
    },
    "ZAR": {
        "name": "Rand",
        "symbol": "R"
    },
    "ZAR LSL": {
        "name": "Rand Loti"
    },
    "ZAR NAD": {
        "name": "Rand Namibia Dollar"
    },
    "ZMK": {
        "name": "Zambian Kwacha"
    },
    "ZWL": {
        "name": "Zimbabwe Dollar"
    }
}