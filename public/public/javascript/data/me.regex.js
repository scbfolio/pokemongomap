/**
 * RegexMe from the MeLibs
 * Library that help you - adding a lots of basics regex
 *
 * Version :
 *  - 1.0.0
 *
 * Dependencies :
 *
 * Private Methods :
 *
 * Public Methods :
 *
 * Updates Needed :
 *
 */
(function($, window, document, undefined){
    "use strict";
    /* Create Me reference if does'nt exist */
    if(!window.Me){window.Me = {};}
    /* Initiate it if doesn't exist */
    if(!Me.regex){Me.regex = {};}

    Me.regex.email = {
        //regex: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/
        regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    };

    Me.regex.password = {
        regex: /(?=^.{1,15}$)(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[\W])(?!.*\s).*$/
    };

    Me.regex.phone_CA = {
        regex: /^(?:\+?1)?[-. ]?\(?[2-9][0-8][0-9]\)?[-. ]?[2-9][0-9]{2}[-. ]?[0-9]{4}$/i,
        mask : "(999) 999-9999"
    }

}(jQuery, window, document));