(function($, window, document, undefined){
    "use strict";
    /* Create Me reference if does'nt exist */
    if(!window.Me){window.Me = {};}
    /* Initiate it if doesn't exist */
    if(!Me.regex){Me.regex = {};}

    Me.regex.zipcode = {};

    Me.regex.zipcode.getByCountry = function(countryCode) {
        var countryCodeString = (countryCode) ? countryCode : "";
        var data = Me.regex.zipcode.data.countries[countryCodeString.toUpperCase()];
        if (!data) {return {};}
        var newData = $.extend({}, data);
        newData.regex = new RegExp("^" + newData.regex + "$", "i");
        return newData;
    };

    Me.regex.zipcode.data = {
        "countries": {
            "GB": {
                "regex": "GIR[ ]?0AA|((AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\\d{1,4}"
            },
            "JE": {
                "regex": "JE\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}"
            },
            "GG": {
                "regex": "GY\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}"
            },
            "IM": {
                "regex": "IM\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}"
            },
            "US": {
                "regex": "\\d{5}([ \\-]\\d{4})?"
            },
            "CA": {
                "regex": "[ABCEGHJKLMNPRSTVXY]\\d[ABCEGHJ-NPRSTV-Z][ ]?\\d[ABCEGHJ-NPRSTV-Z]\\d",
                "mask" : "@9@ 9@9"
            },
            "DE": {
                "regex": "\\d{5}"
            },
            "JP": {
                "regex": "\\d{3}-\\d{4}"
            },
            "FR": {
                "regex": "\\d{2}[ ]?\\d{3}"
            },
            "AU": {
                "regex": "\\d{4}"
            },
            "IT": {
                "regex": "\\d{5}"
            },
            "CH": {
                "regex": "\\d{4}"
            },
            "AT": {
                "regex": "\\d{4}"
            },
            "ES": {
                "regex": "\\d{5}"
            },
            "NL": {
                "regex": "\\d{4}[ ]?[A-Z]{2}"
            },
            "BE": {
                "regex": "\\d{4}"
            },
            "DK": {
                "regex": "\\d{4}"
            },
            "SE": {
                "regex": "\\d{3}[ ]?\\d{2}"
            },
            "NO": {
                "regex": "\\d{4}"
            },
            "BR": {
                "regex": "\\d{5}[\\-]?\\d{3}"
            },
            "PT": {
                "regex": "\\d{4}([\\-]\\d{3})?"
            },
            "FI": {
                "regex": "\\d{5}"
            },
            "AX": {
                "regex": "22\\d{3}"
            },
            "KR": {
                "regex": "\\d{3}[\\-]\\d{3}"
            },
            "CN": {
                "regex": "\\d{6}"
            },
            "TW": {
                "regex": "\\d{3}(\\d{2})?"
            },
            "SG": {
                "regex": "\\d{6}"
            },
            "DZ": {
                "regex": "\\d{5}"
            },
            "AD": {
                "regex": "AD\\d{3}"
            },
            "AR": {
                "regex": "([A-HJ-NP-Z])?\\d{4}([A-Z]{3})?"
            },
            "AM": {
                "regex": "(37)?\\d{4}"
            },
            "AZ": {
                "regex": "\\d{4}"
            },
            "BH": {
                "regex": "((1[0-2]|[2-9])\\d{2})?"
            },
            "BD": {
                "regex": "\\d{4}"
            },
            "BB": {
                "regex": "(BB\\d{5})?"
            },
            "BY": {
                "regex": "\\d{6}"
            },
            "BM": {
                "regex": "[A-Z]{2}[ ]?[A-Z0-9]{2}"
            },
            "BA": {
                "regex": "\\d{5}"
            },
            "IO": {
                "regex": "BBND 1ZZ"
            },
            "BN": {
                "regex": "[A-Z]{2}[ ]?\\d{4}"
            },
            "BG": {
                "regex": "\\d{4}"
            },
            "KH": {
                "regex": "\\d{5}"
            },
            "CV": {
                "regex": "\\d{4}"
            },
            "CL": {
                "regex": "\\d{7}"
            },
            "CR": {
                "regex": "\\d{4,5}|\\d{3}-\\d{4}"
            },
            "HR": {
                "regex": "\\d{5}"
            },
            "CY": {
                "regex": "\\d{4}"
            },
            "CZ": {
                "regex": "\\d{3}[ ]?\\d{2}"
            },
            "DO": {
                "regex": "\\d{5}"
            },
            "EC": {
                "regex": "([A-Z]\\d{4}[A-Z]|(?:[A-Z]{2})?\\d{6})?"
            },
            "EG": {
                "regex": "\\d{5}"
            },
            "EE": {
                "regex": "\\d{5}"
            },
            "FO": {
                "regex": "\\d{3}"
            },
            "GE": {
                "regex": "\\d{4}"
            },
            "GR": {
                "regex": "\\d{3}[ ]?\\d{2}"
            },
            "GL": {
                "regex": "39\\d{2}"
            },
            "GT": {
                "regex": "\\d{5}"
            },
            "HT": {
                "regex": "\\d{4}"
            },
            "HN": {
                "regex": "(?:\\d{5})?"
            },
            "HU": {
                "regex": "\\d{4}"
            },
            "IS": {
                "regex": "\\d{3}"
            },
            "IN": {
                "regex": "\\d{6}"
            },
            "ID": {
                "regex": "\\d{5}"
            },
            "IL": {
                "regex": "\\d{5}"
            },
            "JO": {
                "regex": "\\d{5}"
            },
            "KZ": {
                "regex": "\\d{6}"
            },
            "KE": {
                "regex": "\\d{5}"
            },
            "KW": {
                "regex": "\\d{5}"
            },
            "LA": {
                "regex": "\\d{5}"
            },
            "LV": {
                "regex": "\\d{4}"
            },
            "LB": {
                "regex": "(\\d{4}([ ]?\\d{4})?)?"
            },
            "LI": {
                "regex": "(948[5-9])|(949[0-7])"
            },
            "LT": {
                "regex": "\\d{5}"
            },
            "LU": {
                "regex": "\\d{4}"
            },
            "MK": {
                "regex": "\\d{4}"
            },
            "MY": {
                "regex": "\\d{5}"
            },
            "MV": {
                "regex": "\\d{5}"
            },
            "MT": {
                "regex": "[A-Z]{3}[ ]?\\d{2,4}"
            },
            "MU": {
                "regex": "(\\d{3}[A-Z]{2}\\d{3})?"
            },
            "MX": {
                "regex": "\\d{5}"
            },
            "MD": {
                "regex": "\\d{4}"
            },
            "MC": {
                "regex": "980\\d{2}"
            },
            "MA": {
                "regex": "\\d{5}"
            },
            "NP": {
                "regex": "\\d{5}"
            },
            "NZ": {
                "regex": "\\d{4}"
            },
            "NI": {
                "regex": "((\\d{4}-)?\\d{3}-\\d{3}(-\\d{1})?)?"
            },
            "NG": {
                "regex": "(\\d{6})?"
            },
            "OM": {
                "regex": "(PC )?\\d{3}"
            },
            "PK": {
                "regex": "\\d{5}"
            },
            "PY": {
                "regex": "\\d{4}"
            },
            "PH": {
                "regex": "\\d{4}"
            },
            "PL": {
                "regex": "\\d{2}-\\d{3}"
            },
            "PR": {
                "regex": "00[679]\\d{2}([ \\-]\\d{4})?"
            },
            "RO": {
                "regex": "\\d{6}"
            },
            "RU": {
                "regex": "\\d{6}"
            },
            "SM": {
                "regex": "4789\\d"
            },
            "SA": {
                "regex": "\\d{5}"
            },
            "SN": {
                "regex": "\\d{5}"
            },
            "SK": {
                "regex": "\\d{3}[ ]?\\d{2}"
            },
            "SI": {
                "regex": "\\d{4}"
            },
            "ZA": {
                "regex": "\\d{4}"
            },
            "LK": {
                "regex": "\\d{5}"
            },
            "TJ": {
                "regex": "\\d{6}"
            },
            "TH": {
                "regex": "\\d{5}"
            },
            "TN": {
                "regex": "\\d{4}"
            },
            "TR": {
                "regex": "\\d{5}"
            },
            "TM": {
                "regex": "\\d{6}"
            },
            "UA": {
                "regex": "\\d{5}"
            },
            "UY": {
                "regex": "\\d{5}"
            },
            "UZ": {
                "regex": "\\d{6}"
            },
            "VA": {
                "regex": "120"
            },
            "VE": {
                "regex": "\\d{4}"
            },
            "ZM": {
                "regex": "\\d{5}"
            },
            "AS": {
                "regex": "96799"
            },
            "CC": {
                "regex": "6799"
            },
            "CK": {
                "regex": "\\d{4}"
            },
            "RS": {
                "regex": "\\d{6}"
            },
            "ME": {
                "regex": "8\\d{4}"
            },
            "CS": {
                "regex": "\\d{5}"
            },
            "YU": {
                "regex": "\\d{5}"
            },
            "CX": {
                "regex": "6798"
            },
            "ET": {
                "regex": "\\d{4}"
            },
            "FK": {
                "regex": "FIQQ 1ZZ"
            },
            "NF": {
                "regex": "2899"
            },
            "FM": {
                "regex": "(9694[1-4])([ \\-]\\d{4})?"
            },
            "GF": {
                "regex": "9[78]3\\d{2}"
            },
            "GN": {
                "regex": "\\d{3}"
            },
            "GP": {
                "regex": "9[78][01]\\d{2}"
            },
            "GS": {
                "regex": "SIQQ 1ZZ"
            },
            "GU": {
                "regex": "969[123]\\d([ \\-]\\d{4})?"
            },
            "GW": {
                "regex": "\\d{4}"
            },
            "HM": {
                "regex": "\\d{4}"
            },
            "IQ": {
                "regex": "\\d{5}"
            },
            "KG": {
                "regex": "\\d{6}"
            },
            "LR": {
                "regex": "\\d{4}"
            },
            "LS": {
                "regex": "\\d{3}"
            },
            "MG": {
                "regex": "\\d{3}"
            },
            "MH": {
                "regex": "969[67]\\d([ \\-]\\d{4})?"
            },
            "MN": {
                "regex": "\\d{6}"
            },
            "MP": {
                "regex": "9695[012]([ \\-]\\d{4})?"
            },
            "MQ": {
                "regex": "9[78]2\\d{2}"
            },
            "NC": {
                "regex": "988\\d{2}"
            },
            "NE": {
                "regex": "\\d{4}"
            },
            "VI": {
                "regex": "008(([0-4]\\d)|(5[01]))([ \\-]\\d{4})?"
            },
            "PF": {
                "regex": "987\\d{2}"
            },
            "PG": {
                "regex": "\\d{3}"
            },
            "PM": {
                "regex": "9[78]5\\d{2}"
            },
            "PN": {
                "regex": "PCRN 1ZZ"
            },
            "PW": {
                "regex": "96940"
            },
            "RE": {
                "regex": "9[78]4\\d{2}"
            },
            "SH": {
                "regex": "(ASCN|STHL) 1ZZ"
            },
            "SJ": {
                "regex": "\\d{4}"
            },
            "SO": {
                "regex": "\\d{5}"
            },
            "SZ": {
                "regex": "[HLMS]\\d{3}"
            },
            "TC": {
                "regex": "TKCA 1ZZ"
            },
            "WF": {
                "regex": "986\\d{2}"
            },
            "XK": {
                "regex": "\\d{5}"
            },
            "YT": {
                "regex": "976\\d{2}"
            }
        }
    };
}(jQuery, window, document));