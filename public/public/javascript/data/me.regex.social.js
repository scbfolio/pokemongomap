(function($, window, document, undefined) {
    "use strict";
    /* Create Me reference if does'nt exist */
    if(!window.Me){window.Me = {};}
    /* Initiate it if doesn't exist */
    if(!Me.regex){Me.regex = {};}

    Me.regex.socials = {};

    Me.regex.socials.get = function(social) {
        var socialString = (social) ? social : "";
        return Me.regex.socials.data[socialString.toUpperCase()];
    };

    Me.regex.socials.data = {
        facebook:   {regex:/^.*facebook\.com\/(?:(?:\w\.)*#!\/)?(?:pages\/)?(?:[\w\-\.]*\/)*([\w\-\.]*)/},
        twitter:    {regex:/^(http\:\/\/|https\:\/\/)?(?:www\.)?twitter\.com\/(#!\/)?[a-zA-Z0-9_]+/},
        pinterest:  {regex:/^.*pinterest\.com\/(#!\/)?[a-zA-Z0-9_]+/},
        linkedin:   {regex:/^((https?:\/\/)?((www|\w\w)\.)?linkedin\.com\/)((([\w]{2,3})?)|([^\/]+\/(([\w-\sàáâãäåçèéêëìíîïðòóôõöùúûüýÿ|\d-&#?=])+\/?){1,}))$/},
        dribbble:   {regex:/^(http\:\/\/|https\:\/\/)?(?:www\.)?dribbble\.com\/(#!\/)?[a-zA-Z0-9_]+/},
        googleplus: {regex:/^(http\:\/\/|https\:\/\/)?(?:www\.)?plus\.google\.com\/.?\/?.?\/?([0-9]*)/},
        behance:    {regex:/^(http\:\/\/|https\:\/\/)?(?:www\.)?behance\.net\/(#!\/)?[a-zA-Z0-9_]+/},
        skype:      {regex:/^[a-z][a-z0-9\.,\-_]{5,31}$/i},
        vimeo:      {regex:/^.*(vimeo\.com\/)(#!\/)?[a-zA-Z0-9_]+/},
        imdb:       {regex:/^(http\:\/\/)?(www\.)?(imdb\.com\/)(#!\/)?[a-zA-Z0-9_]+/},
        tumblr:     {regex:/^(http\:\/\/).*(tumblr\.com)(#!\/)?[a-zA-Z0-9_]?/}
    };
}(jQuery, window, document));