(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserFactory', UserFactory);

    function UserFactory($firebaseObject) {
        return $firebaseObject.$extend({
            getData: function(){
                var clone = angular.extend({}, this, {});

                if(typeof clone['$$conf'] !== 'undefined'){
                    delete clone['$$conf'];
                }

                if(typeof clone['$id'] !== 'undefined'){
                    delete clone['$id'];
                }

                if(typeof clone['$priority'] !== 'undefined'){
                    delete clone['$priority'];
                }

                return clone;
            },
            validate: function() {
                var valid = true;

                if (!this.firstname) {valid = false;}
                if (!this.lastname) {valid = false;}
                if (!this.email) {valid = false;}
                if (!this.language) {valid = false;}
                if (!this.status) {valid = false;}
                if (!this.created) {valid = false;}

                return valid;
            }
        });
    }
})();