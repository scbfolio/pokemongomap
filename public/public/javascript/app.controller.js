(function() {
    'use strict';

    angular
        .module('app')
        .controller('AppCtrl', AppCtrl);

    function AppCtrl($scope, $rootScope, $state, $stateParams, $translate, $window, AuthenticationService) {
        var vm       = this;
        vm.appLoaded = false;
        vm.taskIndex = 0;
        vm.working   = [];

        $rootScope.globals   = {};
        $rootScope.settings  = SETTINGS;
        $rootScope.lang      = SETTINGS.LANGUAGE;
        $rootScope.languages = SETTINGS.LANGUAGES;
        $rootScope.lastState = null;

        $rootScope.setLanguages           = setLanguages;
        $rootScope.getEntryByID           = getEntryByID;
        $rootScope.getLength              = getLength;

        $rootScope.taskInit               = taskInit;
        $rootScope.taskSuccess            = taskSuccess;
        $rootScope.taskError              = taskError;
        $rootScope.taskStart              = taskStart;
        $rootScope.taskEnd                = taskEnd;

        $rootScope.removeGlobalsRef       = removeGlobalsRef;
        $rootScope.setGlobalsRef          = setGlobalsRef;

        vm.sendBackEvent        = sendBackEvent;
        vm.setupRegex           = setupRegex;
        vm.onAuthChanged        = onAuthChanged;
        vm.stateChangeStart     = stateChangeStart;
        vm.stateChangeError     = stateChangeError;
        vm.stateChangeSuccess   = stateChangeSuccess;

        init();

        function init() {
            $rootScope.setLanguages($rootScope.lang);

            $scope.$on('send_event', vm.sendBackEvent);
            vm.setupRegex();
            /*AuthenticationService.GetFirebase().onAuth(vm.onAuthChanged);*/
            $scope.$on('$stateChangeStart',   vm.stateChangeStart);
            $scope.$on('$stateChangeError',   vm.stateChangeError);
            $scope.$on('$stateChangeSuccess', vm.stateChangeSuccess);

            angular.element($window).bind('resize', function() {$scope.$broadcast('resizeHandler');});

            vm.appLoaded = true;
        }

        function setLanguages(lang) {
            $translate.use(lang);
            moment.locale(lang);
        }

        function getEntryByID(array, ID) {
            var chosenItem = null;
            $.each(array, function(index, value) {
                if (value.$id == ID) {chosenItem = {key:index, data:value}; return false;}
            });
            return chosenItem;
        }

        function getLength(obj) {
            if(typeof obj === 'object'){
                return Object.keys(obj).length;
            } else {
                return -1;
            }
        }

        function taskInit(taskName) {
            var data = {
                index: vm.taskIndex,
                value: taskName
            };
            $rootScope.taskStart(data);

            vm.taskIndex ++;
            return data;
        }

        function taskSuccess(task, callback) {
            return function() {
                if (callback && typeof callback === 'function') {
                    callback.call(this, task);
                }
                $rootScope.taskEnd(task);
            };
        }

        function taskError(task, callback) {
            return function() {
                task.error = true;
                if (callback && typeof callback === 'function') {
                    callback.call(this, task);
                }
                $rootScope.taskEnd(task);
            };
        }

        function taskStart(task) {
            if(Object.keys(vm.working).length <= 0){
                //console.info('----- TaskManager start -----');
            }
            vm.working['task-' + task.index] = task.value;
        }

        function taskEnd(task) {
            if(vm.working['task-' + task.index]) {
                delete vm.working['task-' + task.index];
            }

            if (task.error) {
                //console.warn('task:' + task.index + ' - ' + task.value + 'has failed.');
            } else {
                //console.log('task:' + task.index + ' - ' + task.value + 'has successed.');
            }

            if(Object.keys(vm.working).length <= 0){
                //console.info('----- TaskManager stop -----');
            }
        }

        function removeGlobalsRef(name) {
            if ($rootScope.globals[name]) {
                $rootScope.globals[name].$destroy();
                delete $rootScope.globals[name];
                vm.sendBackEvent(null, {e: name + '_destroyed'});
            }
        }

        function setGlobalsRef(name, ref, callback) {
            var refLoadedCallBack = (typeof callback === 'function') ? callback : function(){};
            $rootScope.removeGlobalsRef(name);

            $rootScope.globals[name] = ref;
            $rootScope.globals[name].$watch(function() {vm.sendBackEvent(null, {e: name + '_changed'});});
            $rootScope.globals[name].$loaded().then(refLoadedCallBack);
        }

        function sendBackEvent(e, result) {
            var data = (result.data) ? result.data : null;
            $scope.$broadcast(result.e, data);
        }

        function setupRegex() {
            $rootScope.regex          = {};
            $rootScope.regex.email    = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            $rootScope.regex.password = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
        }

        function onAuthChanged(authData) {
            if (!authData) {
                delete $rootScope.globals.auth;
                if (vm.appLoaded) {$state.go('app.auth.login');}
            }
            if (!vm.appLoaded) {vm.appLoaded = true;}
        }

        function stateChangeStart(event, toState, toParams) {
            var data = (toState.data) ? toState.data : {};
            var authorization = data.authorization;

            if (!$rootScope.globals.auth && authorization) {
                event.preventDefault();
                $rootScope.returnToState = toState.url;
                $rootScope.returnToStateParams = toParams;

                var params = $.extend({}, $stateParams);
                $state.go('app.auth.login', params, false);
            }
        }

        function stateChangeError(event, toState, toParams, fromState, fromParams, error) {
            event.preventDefault();
            var params = $.extend({}, {lang: $rootScope.lang}, fromParams, toParams);

            /* Verify Auth */
            if (error === "AUTH_REQUIRED") {
                $rootScope.redirectState = {
                    name: toState.name,
                    params: toParams
                };
                $state.go("app.auth.login", params, false);
                return;
            }
        }

        function stateChangeSuccess(event, toState, toParams, fromState, fromParams) {
            var obj = {
                toState: toState,
                toParams: toParams,
                fromState: fromState,
                fromParams: fromParams
            };

            $rootScope.lastState = {
                name: fromState.name,
                params: fromParams
            };

            $scope.$broadcast('state_changed');
        }

        function routeIncludes(name, search) {
            return (search.search(name) !== -1);
        }
    }
})();
