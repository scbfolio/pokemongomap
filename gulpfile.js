/* Load plugins */
var gulp   = require('gulp');
var gutil  = require('gulp-util');
var config = require('./assets-config.json');
var path   = require('path');
var File   = require('vinyl');
var fs     = require('vinyl-fs');

var plugins = {
    map:         require('map-stream'),
    concat:      require('gulp-concat'),
    uglify:      require('gulp-uglify'),
    uglifyCSS:   require('gulp-uglifycss'),
    rename:      require('gulp-rename'),
    cache:       require('gulp-cached'),
    less:        require('gulp-less'),
    foxyLess:    require('gulp-foxy-less'),
    compass:     require('gulp-compass'),
    gulpFilter:  require('gulp-filter'),
    fileinclude: require('gulp-file-include'),
    eventStream: require('event-stream')
};

/* Set Paths */
var basePath = __dirname + "/public/";
var paths = {
    less: {
        src:  basePath + 'less/',
        dest: basePath + 'css/'
    },
    js: {
        src:  basePath + 'javascript/',
        dest: basePath + 'javascript/'
    }
};

/* Files that will be parsed */
var files = {
    less:    [paths.less.src + '**/*.less'],
    lessFilters: ['*', 'games/*'],
    js:      [
        "*"
    ],
    vendors: []
};



/* Defaults Parameters */
var params = {
    less: {},
    uglify: {
        mangle: false, // create variable with letters
        compress: {
            sequences     : false,  // join consecutive statements with the “comma operator”
            properties    : false,  // optimize property access: a["foo"] → a.foo
            dead_code     : false,  // discard unreachable code
            drop_debugger : false,  // discard “debugger” statements
            unsafe        : false, // some unsafe optimizations (see below)
            conditionals  : false,  // optimize if-s and conditional expressions
            comparisons   : false,  // optimize comparisons
            evaluate      : false,  // evaluate constant expressions
            booleans      : false,  // optimize boolean expressions
            loops         : false,  // optimize loops
            unused        : false,  // drop unused variables/functions
            hoist_funs    : false,  // hoist function declarations
            hoist_vars    : false, // hoist variable declarations
            if_return     : false,  // optimize if-s followed by return/continue
            join_vars     : false,  // join var declarations
            cascade       : false,  // try to cascade `right` into `left` in sequences
            side_effects  : false,  // drop side-effect-free statements
            warnings      : false,  // warn about potentially dangerous optimizations/code
            global_defs   : {}     // global definitions
        }
    },
    uglifyCSS: {
        maxLineLen: 0 //adds a newline (approx.) every n characters; 0 means no newline and is the default value
    }
};

/* Commons Methods */
var log = function(file, cb) {
    gutil.log(file);
    cb(null, file);
};

var traceError = function(err) {
    console.log(err.message); this.emit('end');
};


/* Htmls Methods */
/*gulp.task('fileinclude', function() {
    var packs = [];
    for(var i in config.html) {
        var currentPack = config.html[i];
        var path = {
            src:  currentPack.base_path + currentPack.path.src,
            dest: currentPack.base_path + currentPack.path.dest
        };
        packs.push(makeHtmlStream(currentPack, path, i));
    }
});

function makeHtmlStream(pack, path, index) {
    var stream = gulp.src([config.html + 'html/layouts/!*.html'])
 .pipe(plugins.fileinclude({
 prefix: '@@',
 basepath: basePath + 'html/templates/'
 }))
 .on('error', function(err){ console.log(err.message); this.emit('end');})
 .pipe(gulp.dest('./'));
    return stream;
}

gulp.task('watch:html', function() {
    gulp.watch([basePath +'html/!**!/!*.html'], ['fileinclude']).on('change', function(evt){changeEvent(evt);});
});*/


/* Scripts Methods */
gulp.task('scripts', function() {
    var packs = [];
    for(var i in config.js) {
        var currentPack = config.js[i];
        var path = {
            src:  currentPack.base_path + currentPack.path.src,
            dest: currentPack.base_path + currentPack.path.dest
        };
        packs.push(makeJavascriptStream(currentPack, path, i));
    }
    return plugins.eventStream.concat(packs);
});

function makeJavascriptStream(pack, path, index) {
    var stream = gulp.src(pack.combined, {cwd:path.src})
        .pipe(plugins.map(log))
        .pipe(plugins.concat('combined.' + index + '.js'))
        .on('error', traceError)
        .pipe(gulp.dest(path.dest))
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(plugins.uglify(params.uglify))
        .on('error', traceError)
        .pipe(gulp.dest(path.dest));
    return stream;
}

/* Less Methods */
gulp.task('css', ['less'], function() {
    var packs = [];
    for(var i in config.css) {
        var currentPack = config.css[i];
        var path = {
            src:  currentPack.base_path + currentPack.path.src,
            dest: currentPack.base_path + currentPack.path.dest
        };
        packs.push(makeCssStream(currentPack, path, i));
    }
    return plugins.eventStream.concat(packs);
});

function makeCssStream(pack, path, index) {
    var stream = gulp.src(pack.combined, {cwd:path.dest})
        .pipe(plugins.map(log))
        .pipe(plugins.concat('combined.' + index + '.css'))
        .on('error', traceError)
        .pipe(gulp.dest(path.dest))
        .pipe(plugins.uglifyCSS(params.uglifyCSS))
        .pipe(plugins.rename({suffix: '.min'}))
        .on('error', traceError)
        .pipe(gulp.dest(path.dest));
    return stream;
}

/* LESS METHODS */
var foxy = plugins.foxyLess();
gulp.task('less', function() {
    var packs = [];
    for(var i in config.less) {
        var currentPack = config.less[i];
        var path = {
            src:  currentPack.base_path + currentPack.path.src,
            dest: currentPack.base_path + currentPack.path.dest
        };
        packs.push(makeLessStream(currentPack, path));
    }
    return plugins.eventStream.concat(packs);
});

function makeLessStream(pack, filePath) {
    var stream = fs.src(pack.watch.files, {cwd:filePath.src})
        .pipe(plugins.cache('less'))
        .pipe(foxy())
        .pipe(plugins.gulpFilter(function(file) {
            var valid = true;
            var fileVinyl = new File({
                cwd: __dirname,
                base: filePath.src,
                path: file.path,
                contents: file.contents
            });

            for (var index in pack.watch.excludes) {
                var regexp = RegExp(pack.watch.excludes[index]);
                if (regexp.test(fileVinyl.relative)) {
                    gutil.log("Exclude because of regexp: " + gutil.colors.magenta(regexp), "filename: " + gutil.colors.magenta(fileVinyl.relative));
                    valid = false;
                }
            }
            return valid;
        }))
        .pipe(plugins.map(log))
        .pipe(plugins.less(params.less))
        .on('error', traceError)
        .pipe(gulp.dest(filePath.dest));
    return stream;
}

gulp.task('watch:less', function() {
    gulp.watch(["**/*.less"], ['less']).on('change', function(evt){changeEvent(evt);});
});

/* SCSS Methods */
gulp.task('scss', function() {
    var packs = [];

    for(var i in config.scss) {
        var currentPack = config.scss[i];

        var path = {
            src:  currentPack.base_path + currentPack.path.src,
            dest: currentPack.base_path + currentPack.path.dest
        };

        packs.push(makeScssStream(currentPack, path));
    }

    return plugins.eventStream.concat(packs);
});

function makeScssStream(pack, filePath) {
    var stream = gulp.src(pack.watch.files)
        //.pipe(plugins.cache('scss'))
        .pipe(plugins.map(log))
        .pipe(plugins.compass({
            css: filePath.dest,
            sass: filePath.src
        }))
        .on('error', traceError)
        .pipe(gulp.dest(filePath.dest));

    return stream;
}

gulp.task('watch:scss', function() {
    gulp.watch(["**/*.scss"], ['scss']).on('change', function(evt){changeEvent(evt);});
});

// Default Task
gulp.task('watch',   ['watch:less', 'watch:scss'], function(){});
gulp.task('deploy',  ['scripts', 'css'], function(){});

// methods to trace what file changed
function changeEvent(evt) {
    //if (evt.type === 'deleted') {delete plugins.cache.caches.less[evt.path];}
    gutil.log('File', gutil.colors.cyan(evt.path), 'was', gutil.colors.magenta(evt.type));
}